local lib = LibStub:NewLibrary("LibCompactCurrentDateTime-1.0", 1)

if not lib then return end -- already loaded and no upgrade necessary

----
-- Thanks to the developer of the addon Shinies for creating this code, lovingly pasted below.
----

function lib:GetCurrentDate()
  local a = {}
  
  local today = GetTodaysDate()
  
  a.day   = today.todaysDay
  a.month = today.todaysMonth
  a.year  = today.todaysYear
  
  return a
end

function lib:GetCurrentTime()
  local a = {}
  
  local time = GetComputerTime()
  
  a.second  = time % 60
  a.minute  = ( ( time - a.second ) / 60 ) % 60
  a.hour    = ( ( ( ( time - a.second ) / 60 - a.minute ) / 60 ) % 60 )
  
  return a
end

function lib:GetCurrentDateTime()
  local time = lib:GetCurrentTime()
  local date = lib:GetCurrentDate()
  
  for k, v in pairs( date )
  do
    time[k] = v 
  end
  
  return time
end

function lib:PackDateTime( dt )
  return string.format( "%s:%s:%s:%s:%s:%s", dt.year, dt.month, dt.day, dt.hour, dt.minute, dt.second )
end

function lib:GetCurrentDateTimePack()
  return lib:PackDateTime( lib:GetCurrentDateTime() )
end
