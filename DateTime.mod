<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" >

	<UiMod name="LibCompactCurrentDateTime" version="1.0.0" date="09/01/2020">
		<Author name="Johndon" email="addon@rorah.com" />
		<Description text="For addon developers to easily get the current date and time and format it into a compact string." />
		<VersionSettings gameVersion="1.4.8" windowsVersion="1.0" />
		<Dependencies>
			<Dependency name="LibStub" />
        </Dependencies>

		<Files>
			<File name="DateTime.lua" />
		</Files>
	</UiMod>
</ModuleFile>